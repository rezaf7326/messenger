package Application.View;

import Application.Controllers.AccessibleSearchResult;
import javafx.fxml.FXMLLoader;
import javafx.scene.AccessibleAction;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.io.IOException;



public class ViewFactory {

    // everybody can access this view factory from everywhere in the project
    public static ViewFactory mainViewFactory = new ViewFactory();

    // LOGIN SCENE ADDRESSES
    private final String LOGIN = "defaultImages3/login.png";
    private final String GO_TO_CREATE_NEW = "defaultImages3/new2.png";

    //private final String EXIT_ = "defaultImages3/";




    // CREATE NEW ACCOUNT SCENE ADDRESSES
    private final String ACCOUNT_IMAGE = "defaultImages3/create new account.png";
    private final String PASSWORD = "defaultImages3/password.png";
    private final String USER_NAME = "defaultImages3/user name.png";
    private final String PICTURE_PATH = "defaultImages3/profile picture.png";
    private final String CREATE_NEW = "defaultImages3/new.png";
    private final String BACK_TO_LOGIN = "defaultImages3/back.png";
    private final String ERROR_ICON = "defaultImages3/error.png";
    private final String NEW_AS_FEMALE = "defaultImages3/new as female.png";
    private final String NEW_AS_MALE = "defaultImages3/new as male.png";




    // MAIN SCENE ICON ADDRESSES
    private final String MAIN_FXML = "mainLayout.fxml";
    private final String MAIN_FXML_COPY = "resources/mainLayout.fxml";
    private final String LOGIN_FXML = "loginLayout.fxml";
    private final String NEW_ACCOUNT_SCENE_FXML = "createAccountLayout.fxml";
    private final String SEARCH_FXML = "searchLayout.fxml";
    private final String DEFAULT_STYLE = "style.css";


    private final String MALE = "defaultImages/male-user.png";
    private final String FEMALE = "defaultImages/female-user.png";
    public final String ONLINE2 = "essentials/online2.png";
    public final String ONLINE3 = "essentials/online3.png";
    public final String ONLINE4 = "essentials/online4.png";
    public final String ONLINE = "essentials/online.png";
    private final String DEFAULT_MENU_IMAGE = "defaultImages3/menu.png";
    private final String SEND_BUBBLE_IMAGE = "defaultImages2/send.png";
    private final String SEND_BUBBLE_IMAGE2 = "defaultImages2/send2.png";
    private final String EMOJI_BUBBLE_IMAGE = "defaultImages3/emoji.png";
    private final String SEARCH_BUBBLE_IMAGE = "defaultImages3/search.png";
    private final String LOG_OUT_FROM_ACCOUNT = "";
    private final String ATTACH_IMAGE = "defaultImages3/attach.png";
    private final String GROUP = "defaultImages2/group-chat.png";
    private final String DOWNLOAD = "defaultImages3/download.png";




    public ImageView SEND_BUTTON;
    public ImageView EMOJI_BUTTON;
    public ImageView ATTACH_BUTTON;
    public ImageView DOWNLOAD_BUTTON;
    public Image SEARCH_VIEW;
    public Image FEMALE_USER;
    public Image MALE_USER;
    public Image GROUP_CHAT;
    //public Image ONLINE_IMAGE;

    public void buildMainSceneImages() {
        Image sendBubbleImage = new Image(getClass().getResourceAsStream(SEND_BUBBLE_IMAGE2));
        SEND_BUTTON = new ImageView(sendBubbleImage);
        SEND_BUTTON.setFitWidth(46);
        SEND_BUTTON.setFitHeight(42);


        Image emojiButton = new Image(getClass().getResourceAsStream(EMOJI_BUBBLE_IMAGE));
        EMOJI_BUTTON =new ImageView(emojiButton);
        EMOJI_BUTTON.setFitHeight(34);
        EMOJI_BUTTON.setFitWidth(35);


        Image attach = new Image(getClass().getResourceAsStream(ATTACH_IMAGE));
        ATTACH_BUTTON = new ImageView(attach);
        ATTACH_BUTTON.setFitHeight(34);
        ATTACH_BUTTON.setFitWidth(35);

        Image downloadButton = new Image(getClass().getResourceAsStream(DOWNLOAD));

        SEARCH_VIEW = new Image(getClass().getResourceAsStream(SEARCH_BUBBLE_IMAGE));

        FEMALE_USER = new Image(getClass().getResourceAsStream(FEMALE));
        MALE_USER = new Image(getClass().getResourceAsStream(MALE));

        GROUP_CHAT = new Image(getClass().getResourceAsStream(GROUP));

        //ONLINE_IMAGE = new Image (getClass().getResourceAsStream(ONLINE4));
    }




    public Image ACCOUNT_IMG;
    public Image PASSWORD_IMG;
    public Image USERNAME_IMG;
    public Image PROF_PIC_PATH_IMG;
    public ImageView CREATE_ACCOUNT;
    public ImageView ERROR_ON_CREATING;
    public ImageView BACK_BUTTON;
    public ImageView NEW_FEMALE_BUTTON;
    public ImageView NEW_MALE_BUTTON;

    public void buildCreateNewAccountSceneImages () {

        ACCOUNT_IMG = new Image(getClass().getResourceAsStream(ACCOUNT_IMAGE));

        PASSWORD_IMG = new Image(getClass().getResourceAsStream(PASSWORD));

        USERNAME_IMG = new Image(getClass().getResourceAsStream(USER_NAME));

        PROF_PIC_PATH_IMG = new Image(getClass().getResourceAsStream(PICTURE_PATH));

        CREATE_ACCOUNT = new ImageView(new Image(getClass().getResourceAsStream(CREATE_NEW)));
        CREATE_ACCOUNT.setFitHeight(55);
        CREATE_ACCOUNT.setFitWidth(55);

        ERROR_ON_CREATING = new ImageView(new Image(getClass().getResourceAsStream(ERROR_ICON)));
        ERROR_ON_CREATING.setFitHeight(55);
        ERROR_ON_CREATING.setFitWidth(55);

        BACK_BUTTON = new ImageView(new Image(getClass().getResourceAsStream(BACK_TO_LOGIN)));
        BACK_BUTTON.setFitHeight(30);
        BACK_BUTTON.setFitWidth(30);

        NEW_FEMALE_BUTTON = new ImageView(new Image(getClass().getResourceAsStream(NEW_AS_FEMALE)));
        NEW_FEMALE_BUTTON.setFitWidth(66);
        NEW_FEMALE_BUTTON.setFitHeight(66);

        NEW_MALE_BUTTON = new ImageView(new Image(getClass().getResourceAsStream(NEW_AS_MALE)));
        NEW_MALE_BUTTON.setFitWidth(66);
        NEW_MALE_BUTTON.setFitHeight(66);
    }


    public Scene getMainScene () {
        return initializeScene(MAIN_FXML);
    }

    public Scene getLoginScene () {
        return initializeScene (LOGIN_FXML);
    }

    public Scene getCreateNewAccountScene () {
        return initializeScene(NEW_ACCOUNT_SCENE_FXML);
    }

    public Scene getSearchScene () {
        return initializeScene(SEARCH_FXML);
    }


    private Scene initializeScene (String fxmlFile) {

        FXMLLoader loader;
        Parent parent;
        Scene scene;

        try {
            loader = new FXMLLoader(getClass().getResource(fxmlFile));
            parent = loader.load();
            if (fxmlFile.equals(MAIN_FXML)){
                AccessibleSearchResult.mainLoader = loader;
            }
        }catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        scene = new Scene(parent);
        scene.getStylesheets().add(getClass().getResource(DEFAULT_STYLE).toExternalForm());
        return scene;
    }
}
