package Application.Network.Utils;

import Application.Network.ConnectToServer;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.Base64;

public class BASE_64 {

    private static ConnectToServer connection = ConnectToServer.connectToServer;

    public static FileInputStream decodeBase64ToStream (String fileString) throws IOException {

        if (fileString == null) { return null; }
        String directory = "D:\\Java projects\\FX\\Messenger client\\src\\Application\\View\\images\\temp\\";
        //FileUtils.cleanDirectory(new File(directory));
        String fileName = "temp file";
        File file = new File (directory + fileName);
        if (file.delete()) {
            System.out.println("temp file deleted.");
        }else {
            System.out.println("temp file created...");
        }

        File tempFile = new File(directory + fileName);
        FileOutputStream fileOut = new FileOutputStream(tempFile);
        IOUtils.copy(new ByteArrayInputStream(Base64.getDecoder().decode(fileString)), fileOut);//check out
        return new FileInputStream(new File(directory + fileName));
    }

    public static String encodeFileToBase64 (File file) throws IOException {
        FileInputStream fileIn = new FileInputStream(file);
        byte [] chunk = new byte[connection._11000];
        ByteArrayOutputStream allBytes = new ByteArrayOutputStream();
        for (int i = 0; i < connection._1000; i++) {
            if (fileIn.read(chunk) != -1) {
                allBytes.write(chunk);
            }else {
                break;
            }
        }
        return Base64.getEncoder().encodeToString(allBytes.toByteArray());
    }
}
