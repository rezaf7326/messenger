package Application.Network;

import Application.Controllers.MainController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.NewMessage;
import model.OnlineStatus;
import model.TypingStatus;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class ConnectToServer implements Closeable{

//  The Only Instance of this class:
    public static ConnectToServer connectToServer;
    static {
        try {
            connectToServer = new ConnectToServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public final int SIZE_OF_16MB = 16_000_000;
    public final int SIZE_OF_11MB = 11_000_000;
    public final int SIZE_OF_10MB = 10_000_000;
    public final int _1000 = 1000;
    public final int _11000 = 11000;

    private final Socket twoWaySocket;
    private final Socket oneWaySocket;
    private final ObjectOutputStream twoWayOutput;
    private final ObjectInputStream twoWayInput;
    private final ObjectOutputStream oneWayConnectToServer;
    private final ObjectInputStream oneWayInput;
    private final BufferedOutputStream bufferedOutput1;
    private final BufferedInputStream bufferedReader1;
    private final BufferedOutputStream bufferedOutput2;
    private final BufferedInputStream bufferedReader2;


    public ConnectToServer() throws IOException {
        this.twoWaySocket = new Socket("localhost" , 30003);
        this.oneWaySocket = new Socket("localhost" , 30003);
        bufferedOutput1 = new BufferedOutputStream(twoWaySocket.getOutputStream());
        bufferedReader1 = new BufferedInputStream(twoWaySocket.getInputStream());
        bufferedOutput2 = new BufferedOutputStream(oneWaySocket.getOutputStream());
        bufferedReader2 = new BufferedInputStream(oneWaySocket.getInputStream());
        this.twoWayOutput = new ObjectOutputStream(bufferedOutput1);
        twoWayOutput.flush();
        this.twoWayInput = new ObjectInputStream(bufferedReader1);
        this.oneWayConnectToServer = new ObjectOutputStream(bufferedOutput2);
        oneWayConnectToServer.flush();
        this.oneWayInput = new ObjectInputStream(bufferedReader2);
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Two Way Socket Usage:
    public synchronized Object getData() throws IOException, ClassNotFoundException {
        System.out.println("getting data...");
        long initial = System.currentTimeMillis() / 1000;
        while (true) {
            if (bufferedReader1.available() > 0) {
                System.out.println("data");
                return twoWayInput.readObject();
            }
            long now = System.currentTimeMillis() / 1000;
            if (now - initial > 5) {
                System.out.println("server not responding!");
                return null;
            }
        }
    }

    public synchronized void sendData(Object o) throws IOException {
        twoWayOutput.writeObject(o);
        twoWayOutput.flush();
    }

    @Override
    public void close() throws IOException {
        this.twoWaySocket.close();
        this.twoWayOutput.close();
        this.twoWayInput.close();
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//  One Way:
    public volatile ObservableList<NewMessage> observableNewMessages = FXCollections.observableArrayList(new ArrayList<>());
    public void incomingMessage() {
        Thread incoming = new Thread() {
            @Override
            public void run() {
                System.out.println("incoming messages running<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                while (true) {
                    try {
                        if (bufferedReader2.available() > 0) {
                            System.out.println("new Object from incoming messages");
                            Object obj = oneWayInput.readObject();
                            if (obj instanceof NewMessage) {
                                System.out.println("NewMessage from incoming messages");
                                observableNewMessages.add((NewMessage) obj);
                            } /*else if (obj instanceof OnlineStatus) {

                            }*/ /*else if (obj instanceof TypingStatus) {
                                controller.thisContactIsTyping();
                            }*/
                        }
                    } catch (IOException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        incoming.start();
        //incoming.join();todo CHECK
    }
    public void authentication (String accountName) throws IOException {
        oneWayConnectToServer.writeObject("1W: " + accountName);
        oneWayConnectToServer.flush();
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////
}
