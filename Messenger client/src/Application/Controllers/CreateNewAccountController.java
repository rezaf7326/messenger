package Application.Controllers;

import Application.MainClient;
import Application.Model.User;
import Application.Network.ConnectToServer;
import Application.Network.Utils.BASE_64;
import Application.View.ViewFactory;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.NewAccount;
import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;


public class CreateNewAccountController implements Initializable {

    @FXML
    private TextField accountName;

    @FXML
    private ImageView accountIcon;

    @FXML
    private PasswordField password;

    @FXML
    private ImageView passIcon;

    @FXML
    private TextField userName;

    @FXML
    private ImageView userIcon;

    @FXML
    private TextField pathTProfilePicture;

    @FXML
    void browseAction() {
        pathTProfilePicture.setText(null);

        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();

        File selectedFile = fileChooser.showOpenDialog(stage);
        if (selectedFile != null) {
            long size = selectedFile.length();
            if (size > connection.SIZE_OF_11MB) {
                pathTProfilePicture.setText("sorry cannot be more than 11 MB");
                accountName.setStyle("-fx-text-inner-color: red;");
                return;
            }
            pathTProfilePicture.setText(selectedFile.toString());
            try {
                FileInputStream fileIn = new FileInputStream(selectedFile);
                profilePic = new Image(fileIn);
                stringProfilePic = BASE_64.encodeFileToBase64(selectedFile);
            } catch (IOException e) {
                System.out.println("\nProblem reading the file...\n");
                profilePic = null;
            }
        }
    }

    @FXML
    void removeSelectedPicture() {
        profilePic = null;
        pathTProfilePicture.setText(null);
        pathTProfilePicture.setStyle("-fx-text-inner-color: black;");
        fileTooLarge = false;
    }

    @FXML
    private ImageView profilePicIcon;

    @FXML
    private Button newAccountButton;

    @FXML
    private Button backToLogin;

    @FXML
    private Button maleButton;

    @FXML
    private Button femaleButton;

    @FXML
    private Text showGender;

    @FXML
    private Text fillAllTheFieldsPrompt;

    @FXML
    private Text showProblemConnectingToServer;




    @FXML
    void closePlatform() {
        MainClient.closeConnection();
        Platform.exit();
    }

    @FXML
    void createNewAccount() {
        if (!allNecessaryFieldsFilled()) {
            fillAllTheFieldsPrompt.setVisible(true);
            newAccountButton.setGraphic(mainView.ERROR_ON_CREATING);
            return;
        }
        NewAccount newAccount = profilePic == null? (new NewAccount(gender, accountName.getText(), userName.getText(), password.getText())):
                                                    (new NewAccount(gender, accountName.getText(), userName.getText(), password.getText(), stringProfilePic));
        requestNewAccount(newAccount);
    }

    @FXML
    void femaleButtAction() {
        showGender.setText("Female");
        showGender.setTextAlignment(TextAlignment.CENTER);
        gender = "female";
    }

    @FXML
    void maleButtAction() {
        showGender.setText("Male");
        showGender.setTextAlignment(TextAlignment.CENTER);
        gender = "male";
    }



//  Send "newAccount to server...
    private void requestNewAccount (NewAccount newAccount) {
        try {
            connection.sendData(newAccount);
            if ((boolean)connection.getData()) {
                updateUserAsNewAccount();
                MainClient.showMainStage();
                MainClient.closeCreateNewAccountStage();
            }else {
                accountName.setText("This account name already exists!");
                accountName.setStyle("-fx-text-inner-color: red;");
                newAccountButton.setGraphic(mainView.ERROR_ON_CREATING);
            }
        } catch (Exception e) {
            showProblemConnectingToServer.setVisible(true);
        }
    }

    private void updateUserAsNewAccount() {
        User.accountName = accountName.getText();
        User.username = userName.getText();
        User.gender = gender;
        if (profilePic != null) {
            User.profPic = profilePic;
        }
    }



    private Image profilePic;
    private String stringProfilePic;
    private boolean fileTooLarge = false;
    private String gender = "";

    private boolean allNecessaryFieldsFilled () {
        if (accountName.getText().equals("") || accountName.getText() == null) {
            newAccountButton.setGraphic(mainView.ERROR_ON_CREATING);
            fillAllTheFieldsPrompt.setVisible(true);
            return false;
        }else if (password.getText().equals("") || password.getText() == null) {
            newAccountButton.setGraphic(mainView.ERROR_ON_CREATING);
            fillAllTheFieldsPrompt.setVisible(true);
            return false;
        }else if (userName.getText().equals("") || userName.getText() == null) {
            newAccountButton.setGraphic(mainView.ERROR_ON_CREATING);
            fillAllTheFieldsPrompt.setVisible(true);
            return false;
        }else if (gender.equals("")) {
            newAccountButton.setGraphic(mainView.ERROR_ON_CREATING);
            fillAllTheFieldsPrompt.setVisible(true);
            return false;
        }else {
            return true;
        }
    }



    private ViewFactory mainView = ViewFactory.mainViewFactory;
    private final ConnectToServer connection = ConnectToServer.connectToServer;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        fillAllTheFieldsPrompt.setVisible(false);
        showProblemConnectingToServer.setVisible(false);

        mainView.buildCreateNewAccountSceneImages();

        accountIcon.setImage(mainView.ACCOUNT_IMG);
        passIcon.setImage(mainView.PASSWORD_IMG);
        userIcon.setImage(mainView.USERNAME_IMG);
        profilePicIcon.setImage(mainView.PROF_PIC_PATH_IMG);
        femaleButton.setGraphic(mainView.NEW_FEMALE_BUTTON);
        maleButton.setGraphic(mainView.NEW_MALE_BUTTON);
        backToLogin.setGraphic(mainView.BACK_BUTTON);
        newAccountButton.setGraphic(mainView.CREATE_ACCOUNT);

        accountName.setOnMouseClicked(e -> {
            accountName.setText(null);
            newAccountButton.setGraphic(mainView.CREATE_ACCOUNT);
            accountName.setStyle("-fx-text-inner-color: black;");
            fillAllTheFieldsPrompt.setVisible(false);
        });
        accountName.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldStr, String newStr) {
                newAccountButton.setGraphic(mainView.CREATE_ACCOUNT);
                accountName.setStyle("-fx-text-inner-color: black;");
                fillAllTheFieldsPrompt.setVisible(false);
                showProblemConnectingToServer.setVisible(false);
                //Alert //todo replace with Alert
            }
        });

        password.setOnMouseClicked(e -> {
            newAccountButton.setGraphic(mainView.CREATE_ACCOUNT);
            fillAllTheFieldsPrompt.setVisible(false);
        });
        password.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldStr, String newStr) {
                newAccountButton.setGraphic(mainView.CREATE_ACCOUNT);
                fillAllTheFieldsPrompt.setVisible(false);
            }
        });

        userName.setOnMouseClicked(e -> {
            newAccountButton.setGraphic(mainView.CREATE_ACCOUNT);
            fillAllTheFieldsPrompt.setVisible(false);
            showProblemConnectingToServer.setVisible(false);
        });
        userName.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldStr, String newStr) {
                newAccountButton.setGraphic(mainView.CREATE_ACCOUNT);
                fillAllTheFieldsPrompt.setVisible(false);
                showProblemConnectingToServer.setVisible(false);
            }
        });

        pathTProfilePicture.setOnMouseClicked(e -> {
            newAccountButton.setGraphic(mainView.CREATE_ACCOUNT);
            fillAllTheFieldsPrompt.setVisible(false);
            showProblemConnectingToServer.setVisible(false);
            if (fileTooLarge) {
                pathTProfilePicture.setText(null);
                pathTProfilePicture.setStyle("-fx-text-inner-color: black;");
                fileTooLarge = false;
            }
        });
        pathTProfilePicture.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldStr, String newStr) {
                newAccountButton.setGraphic(mainView.CREATE_ACCOUNT);
                fillAllTheFieldsPrompt.setVisible(false);
                showProblemConnectingToServer.setVisible(false);
                if (fileTooLarge) {
                    pathTProfilePicture.setText(null);
                    pathTProfilePicture.setStyle("-fx-text-inner-color: black;");
                    fileTooLarge = false;
                }
            }
        });

        maleButton.setOnMouseClicked(e -> {
            newAccountButton.setGraphic(mainView.CREATE_ACCOUNT);
            fillAllTheFieldsPrompt.setVisible(false);
            showProblemConnectingToServer.setVisible(false);
        });

        femaleButton.setOnMouseClicked(e -> {
            newAccountButton.setGraphic(mainView.CREATE_ACCOUNT);
            fillAllTheFieldsPrompt.setVisible(false);
            showProblemConnectingToServer.setVisible(false);
        });

        backToLogin.setOnMouseClicked(e -> {
            MainClient.showLoginStage();
            MainClient.closeCreateNewAccountStage();
        });
    }
}
