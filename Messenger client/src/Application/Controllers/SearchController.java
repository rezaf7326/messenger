package Application.Controllers;

import Application.MainClient;
import Application.Model.User;
import Application.Network.ConnectToServer;
import model.RequestConversation;
import Application.Network.Utils.BASE_64;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.AllMessages;
import model.ConversationInfo;
import model.MemberInfo;
import model.NewMessage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class SearchController implements Initializable {

    @FXML
    private ListView<Label> searchResultList;

    @FXML
    void finishSearch() {
        MainClient.closeSearchStage();
    }

    @FXML
    public void handleMouseClick(MouseEvent mouseEvent) {

    }

    private final MainController control = AccessibleSearchResult.mainLoader.getController();
    private final ConnectToServer connection = ConnectToServer.connectToServer;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        searchResultList.setStyle("-fx-control-inner-background: #33363b;");

        for (Label item : AccessibleSearchResult.resultList) {
            searchResultList.getItems().add(item);
        }

        searchResultList.setCellFactory(slv -> {
            ListCell<Label> cell = new ListCell<Label>(){
                @Override
                protected void updateItem(Label item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setGraphic(item.getGraphic());
                        setText(item.getText());
                        setTextFill(Color.BEIGE);
                        setFont(Font.font(16));
                    }
                }
            };
            cell.setOnMouseClicked(e -> {
                if (cell.getItem() != null) {
                    boolean targetFound = false;
                    Label memberLabel = cell.getItem();

                    String chosenAccName = memberLabel.getText();  // 2 acc name
                    System.out.println("chosen target acc name: " + chosenAccName);//todo remove
                    String username = AccessibleSearchResult.getUserFromAccName.get(chosenAccName);

                    // check if the chosen target is already among conversations:
                    if (User.conversations != null) {
                        for (ConversationInfo conversation : User.conversations) {
                            for (MemberInfo existingMember : conversation.getMembers()) {
                                if (existingMember.getAccountName().equals(chosenAccName)) {
                                    User.conversations.remove(conversation);
                                    User.conversations.add(0, conversation);
                                    User.currentConversationId = conversation.getId();
                                    AccessibleSearchResult.memberResult = existingMember;
                                    try {
                                        connection.sendData(new RequestConversation(conversation.getId()));
                                        AllMessages messages = (AllMessages) connection.getData();
                                        User.thisConversationMessages = messages.getAllMessages();
                                        Image image = new Image(BASE_64.decodeBase64ToStream(existingMember.getProfilePic()));
                                        ImageView picView = new ImageView(control.formatImage(image, existingMember.getGender()));
                                        picView.setClip(control.formatClip());
                                        control.setupSelectedConversation(picView, existingMember.getUserName());

                                    } catch (IOException | ClassNotFoundException ex) {
                                        ex.printStackTrace();
                                    }
                                    targetFound =true;
                                    break;
                                }
                            }
                            if (targetFound) {
                                System.out.println("this conversation is already created.");   //todo remove
                                break;
                            }
                        }
                    }




                    // if chosen target was not found among existing conversations:(request for him/her)
                    if (!targetFound) {
                        System.out.println("creating new conversation...");   //todo remove
                        try {
                            NewMessage newContact = new NewMessage(User.accountName, chosenAccName, null, new Date(), false);
                            connection.sendData(newContact);
                            ConversationInfo newConversationInfo = (ConversationInfo) connection.getData();
                            User.currentConversationId = newConversationInfo.getId();
                            if (User.conversations == null) {
                                User.conversations = new ArrayList<>();
                            }
                            User.conversations.add(0, newConversationInfo);
                            if (User.contactsAccNames == null) {
                                User.contactsAccNames = new ArrayList<>();
                            }
                            User.contactsAccNames.add(0, chosenAccName);
                            control.makeConversationList();

                            MemberInfo thisMember = newConversationInfo.getMembers().get(0).getAccountName().equals(User.accountName) ?
                                                                newConversationInfo.getMembers().get(1) : newConversationInfo.getMembers().get(0);
                            Image image = thisMember.getProfilePic() != null?
                                    new Image(BASE_64.decodeBase64ToStream(thisMember.getProfilePic())): null;
                            ImageView picView = new ImageView(control.formatImage(image, thisMember.getGender()));
                            picView.setClip(control.formatClip());
                            User.thisConversationMessages.clear();
                            control.setupSelectedConversation(picView, username);
                        } catch (IOException | ClassNotFoundException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
                finishSearch();
            });
            return cell;
        });

        searchResultList.setOnMouseClicked(e -> {
            System.out.println("You clicked on an empty cell on search list");
        });
    }
}
