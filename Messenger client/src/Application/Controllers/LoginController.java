package Application.Controllers;

import Application.MainClient;
import Application.Model.User;
import Application.Network.ConnectToServer;
import Application.Network.Utils.BASE_64;
import Application.View.ViewFactory;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.text.Text;
import model.LoginData;
import model.UserInfo;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    @FXML
    private TextField accountNameField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Text wrongInputsPrompt;

    @FXML
    private Text ServerNotResponding;

    @FXML
    void closePlatform() {
        MainClient.closeConnection();
        Platform.exit();
    }

    @FXML
    void goToCreateNewAccount() {
        MainClient.showCreateNewAccountStage();
        MainClient.closeLoginStage();
    }


    public static final String SETUP_PROFILE_VIEW = "setting up user profile view";
    private ConnectToServer connection = ConnectToServer.connectToServer;
    //private ConnectToServerReceiver connection = ConnectToServerReceiver.ConnectToServerReceiver;


    // server not responding / connection failure / incorrect user\pass
    private boolean loginSuccessful = false;


    private void sendUserAndPassToServer (LoginData loginData) {
        try {
            connection.sendData(loginData);
            Object obj = connection.getData();
            if (obj != null) {
                loginSuccessful = (Boolean) obj;
                if (loginSuccessful) {
                    connection.authentication(accountNameField.getText());
                    System.out.println("authenticated");
                }
            }else {
                ServerNotResponding.setVisible(true);
            }
        } catch (IOException | ClassNotFoundException e) {
            ServerNotResponding.setVisible(true);
        }
    }


    private void setupUserProfileView() {
        try {
            connection.sendData(SETUP_PROFILE_VIEW);
            UserInfo data = (UserInfo) connection.getData();

            User.accountName = data.getAccountName();
            User.username = data.getUserName();
            User.gender = data.getGender();
            if (data.getProfilePic() != null) {
                User.profPic = new Image(BASE_64.decodeBase64ToStream(data.getProfilePic()));
            }else {
                User.profPic = null/*User.gender.equals("male")? ViewFactory.mainViewFactory.MALE_USER:
                                ViewFactory.mainViewFactory.FEMALE_USER*/;
            }
            User.contactsAccNames = data.getContactsAccName();
            User.conversations = data.getConversations();

            MainClient.showMainStage();
            MainClient.closeLoginStage();

        } catch (IOException | ClassNotFoundException e) {
            ServerNotResponding.setVisible(true);
        }
    }


    private void login () {
        LoginData loginData = new LoginData(accountNameField.getText(), passwordField.getText());
        sendUserAndPassToServer(loginData);
        if (loginSuccessful) {
            setupUserProfileView();
        }else if (!ServerNotResponding.isVisible()){
            wrongInputsPrompt.setVisible(true);
        }
    }



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        wrongInputsPrompt.setVisible(false);
        ServerNotResponding.setVisible(false);

        accountNameField.setOnAction(e -> {
           if (accountNameField.getText() != null && !accountNameField.getText().equals("") &&
                   passwordField.getText() != null && !passwordField.getText().equals("")) {
               login();
           }
        });

        passwordField.setOnAction (e -> {
            if (accountNameField.getText() != null && !accountNameField.getText().equals("") &&
                    passwordField.getText() != null && !passwordField.getText().equals("")) {
                login();
            }
        });


        accountNameField.setOnMouseClicked(e -> {
            wrongInputsPrompt.setVisible(false);
            ServerNotResponding.setVisible(false);
        });
        accountNameField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldStr, String newStr) {
                wrongInputsPrompt.setVisible(false);
                ServerNotResponding.setVisible(false);
            }
        });


        passwordField.setOnMouseClicked(e -> {
            wrongInputsPrompt.setVisible(false);
            ServerNotResponding.setVisible(false);
        });
        passwordField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldStr, String newStr) {
                wrongInputsPrompt.setVisible(false);
                ServerNotResponding.setVisible(false);
            }
        });
    }
}
