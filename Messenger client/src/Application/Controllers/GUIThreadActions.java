package Application.Controllers;


import javafx.application.Platform;

public class GUIThreadActions extends Thread {

    // CONSTRUCTOR
    public GUIThreadActions (String action) {
        this.action = action;
    }


    // all actions:
    private String action;
    public static final String IS_TYPING = "typing";
    public static final String IS_SEARCHING = "searching";


    public void performAction () {
        Thread thread = new Thread(this);
        thread.start();
    }


    @Override
    public void run() {
        switch (action) {
            case IS_TYPING:
                isTyping();
            case IS_SEARCHING:
        }
    }


    private void isTyping () {
        MainController.previousTypingThreadRunning.set(true);
        MainController.mainController.showIsTyping.setVisible(true);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        MainController.mainController.showIsTyping.setVisible(false);
        MainController.previousTypingThreadRunning.set(false);
    }
}
