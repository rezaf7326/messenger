package Application.Controllers;

import Application.MainClient;
import Application.Model.Contact;
import Application.Network.ConnectToServer;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import model.RequestConversation;
import javafx.scene.text.Font;
import model.*;
import Application.Network.Utils.BASE_64;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.shape.Rectangle;
import Application.Model.User;
import Application.View.ViewFactory;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;


public class MainController implements Initializable {


    @FXML
    private VBox chatBox;

    @FXML
    private TextField typeHereTextField;

    @FXML
    private TextFlow emojiTextFlow;

    @FXML
    private TextField getUsername;

    @FXML
    private Text showUserName;

    @FXML
    private ImageView profilePicture;

    @FXML
    private Button sendButton;

    @FXML
    private Button buttonEmoji;
    //😊

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private ImageView contactImage;

    @FXML
    private Text contactName;

    @FXML
    protected Text showIsTyping;

    @FXML
    private ImageView contactOnlineStatus;

    @FXML
    private Text onlineText;

    @FXML
    private ListView <Label> chatListView;

    @FXML
    private HBox contactHBox;

    @FXML
    private ImageView searchContactIcon;

    @FXML
    private TextField searchText;

    @FXML
    private Button attachButton;

    @FXML
    private MenuItem muteOption;


    @FXML
    void searchUser() {
        AccessibleSearchResult.resultList.clear();
        String search = "search: " + searchText.getText();
        try {
            connection.sendData(search);
            AccessibleSearchResult.listAllResults((SearchResult) connection.getData());
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("search failed.");
            e.printStackTrace();
        }
        MainClient.showSearchStage();
    }

    @FXML
    void changeProfilePictureAction(ActionEvent event) {

    }

    @FXML
    void muteUnMute(ActionEvent event) {
        if (!MUTE) {
            MUTE = true;
            muteOption.setText("UN MUTE");
        }else {
            MUTE = false;
            muteOption.setText("MUTE");
        }
    }

    @FXML
    void logOutFromAccount(ActionEvent event) {
        //informServerOnLogout();
        User.removeAll();
        MainClient.showLoginStage();
        MainClient.closeMainStage();
    }

    @FXML
    void attachAction() throws IOException {
        if (User.currentConversationId == null) return;
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();

        File selectedFile = fileChooser.showOpenDialog(stage);

        if (selectedFile != null) {
            Message message = new Message();
            message.setSeen(false);
            message.setContainsFile(true);
            message.setSender(User.accountName);
            message.setDate(new Date());
            message.setContent(BASE_64.encodeFileToBase64(selectedFile));
            message.setReceiver(null);
            message.setConversationId(User.currentConversationId);

            putMessageToRight(message);

            String receiverAccName = "";
            for (ConversationInfo conversation: User.conversations) {
                if (conversation.getId() == User.currentConversationId) {
                    receiverAccName = conversation.getMembers().get(0).getAccountName().equals(User.accountName)?
                                        conversation.getMembers().get(1).getAccountName():
                                        conversation.getMembers().get(0).getAccountName();
                }
            }
            NewMessage newMessage = new NewMessage(User.currentConversationId, User.accountName, receiverAccName,
                    BASE_64.encodeFileToBase64(selectedFile), true, new Date());
            sendNewMessageToServer(newMessage);
        }
    }

    @FXML
    void setUsernameOption() {
        showUserName.setText(null);
        getUsername.setVisible(true);
        getUsername.setOnAction(event -> {
            User.username = getUsername.getText();
            showUserName.setText(User.username.equals("")? "User" : User.username);
            getUsername.setVisible(false);
        });
    }

    @FXML
    void buttonEmojiAction() {
        emojiTextFlow.setVisible(!emojiTextFlow.isVisible());
    }


    @FXML
    void showMyProfilePic(MouseEvent event) {
        Stage stage = new Stage();
        BorderPane pane = new BorderPane();
        ImageView profPic = new ImageView(profilePicture.getImage());
        pane.setCenter(profPic);
        Scene imageScene = new Scene(pane, 600, 600);
        stage.setScene(imageScene);
        stage.show();
    }


    //private ImageView storedContactImage;
    @FXML
    void contactImageClicked() {
        Stage stage = new Stage();
        BorderPane pane = new BorderPane();
        ImageView imageView = new ImageView();
        imageView.setImage(contactImage.getImage());
        pane.setCenter(imageView);
        Scene imageScene = new Scene(pane, 600, 600);
        stage.setScene(imageScene);
        stage.show();
    }


    private void updateListViewOnActions () throws IOException {
        for (ConversationInfo conversation: User.conversations) {
            if (conversation.getId() == User.currentConversationId) {
                if (User.conversations.indexOf(conversation) != 0) {
                    User.conversations.remove(conversation);
                    User.conversations.add(0, conversation);
                    makeConversationList();
                }
            }
        }
    }

    private boolean MUTE = false;
    private String SOUND_CLIP_1 ="D:\\Java projects\\FX\\Messenger client\\src\\Application\\Model\\ChatProperties\\sounds\\05swiping_noise.wav";
    private Media media = new Media(new File(SOUND_CLIP_1).toURI().toString());
    private MediaPlayer newMessageSound = new MediaPlayer(media);
    // Messaging:
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    void sendAction(ActionEvent event) throws IOException, InterruptedException {

        //receiver.interrupt();
        if (User.currentConversationId == null) return;

        if (emojiTextFlow.isVisible()) {
            emojiTextFlow.setVisible(false);
        }
        // THIS MESSAGE !containFile: <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        Message message = new Message();
        message.setSeen(false);
        message.setContainsFile(false);
        message.setSender(User.accountName);
        message.setDate(new Date());
        message.setContent(typeHereTextField.getText());
        message.setReceiver(null);
        message.setConversationId(User.currentConversationId);

        putMessageToRight(message);
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        NewMessage newMessage = new NewMessage(User.currentConversationId, User.accountName, null,
                                                typeHereTextField.getText(), false, new Date());
        sendNewMessageToServer(newMessage);
        typeHereTextField.clear();
        updateListViewOnActions();

        //receiveNewMessages();
    }

    private void sendNewMessageToServer (NewMessage newMessage) {
        try {
            connection.sendData(newMessage);
            boolean sentSuccessful = (boolean) connection.getData();
            System.out.println("message sent.");
        } catch (IOException e) {
            System.out.println("sending message failed!");
        } catch (ClassNotFoundException e) {
            System.out.println("server is NOT telling us if message was successfully sent!");
        }
    }

   /* private void receiveNewMessages() throws InterruptedException {
        Thread keepReceiving = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    connection.incomingMessage();
                } catch (IOException | ClassNotFoundException | InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("going to receive new messages...");
                while (true) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (connection.newMessages.size() > 0) {
                        Thread receiveOne = new Thread() {
                            @Override
                            public void run() {  // last message
                                NewMessage newMessage = connection.newMessages.get(connection.newMessages.size() - 1);
                                handleNewMessage(newMessage);
                            }
                        };
                        receiveOne.start();
                        try {
                            receiveOne.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
        keepReceiving.start();
        keepReceiving.join();
    }*/

//  todo ONE SOCKET
    Thread receiver;
    private void getNewMessagesPeriodically () {
        receiver = new Thread () {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(2000);
                        connection.sendData(new RequestConversation(User.currentConversationId));
                        AllMessages messages = (AllMessages) connection.getData();
                        int allMessages = messages.getAllMessages().size();
                        int oldMessages = User.thisConversationMessages.size();
                        int newMessages = 0;
                        if ((newMessages = allMessages - oldMessages) > 0) {
                            User.thisConversationMessages = messages.getAllMessages();
                            int startIndex = allMessages - newMessages;
                            for (; startIndex < messages.getAllMessages().size(); startIndex++) {
                                Message message = messages.getAllMessages().get(startIndex);
                                NewMessage newMessage = new NewMessage(message.getConversationId(),
                                                                        message.getSender(),
                                                                        message.getReceiver(),
                                                                        message.getContent(),
                                                                        message.isContainsFile(),
                                                                        message.getDate());
                                handleNewMessage(newMessage);
                                Thread.sleep(50);
                            }
                        }
                    } catch (IOException | ClassNotFoundException | InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        };
        receiver.start();
    }


    private void handleNewMessage(NewMessage newMessage) throws IOException, ClassNotFoundException {
        if (!MUTE) {
            newMessageSound.setAutoPlay(true);
        }

        if (User.currentConversationId == null || !isInExistingConversations(newMessage.getConversationId())){
            // this new message is from a new conversation todo IMPLEMENT
            //add to top of chatListView + add conversationId to User.getLabelFromId map
                    /*User.conversations.remove(conversation);
                    User.conversations.add(0, conversation);
                    User.currentConversationId = conversation.getId();
                    makeConversationList();*/
            if (!MUTE) {
                newMessageSound.setAutoPlay(true);
            }
            connection.sendData(LoginController.SETUP_PROFILE_VIEW);
            UserInfo userInfo = (UserInfo) connection.getData();
            User.conversations = userInfo.getConversations();
            for (ConversationInfo conversation : User.conversations) {
                if (conversation.getId() == newMessage.getConversationId()) {
                    User.conversations.remove(conversation);
                    User.conversations.add(0, conversation);
                }
            }
            makeConversationList();

        } else if (newMessage.getConversationId() == User.currentConversationId) {
            Message message = new Message();
            message.setContainsFile(newMessage.isContainsFile());
            message.setContent(newMessage.getContent());
            message.setDate(newMessage.getDate());
            message.setReceiver(newMessage.getReceiverAccName()); //User.accountName
            message.setSender(newMessage.getSenderAccName());
            message.setSeen(false);

            putMessageToLeft(message);
        }else if (isInExistingConversations(newMessage.getConversationId())){//todo IMPLEMENT
            // add " *" to username text of the label with the same id + bring to the top:
                   /*User.conversations.remove(conversation);
                    User.conversations.add(0, conversation);
                    User.currentConversationId = conversation.getId();
                    makeConversationList();*/
            if (!MUTE) {
                newMessageSound.setAutoPlay(true);
            }

            //Text notification = new Text (User.getLabelFromId.get(newMessage.getConversationId()).getText());
            for (ConversationInfo conversation : User.conversations) {
                if (conversation.getId() == newMessage.getConversationId()) {
                    User.conversations.remove(conversation);
                    User.conversations.add(0, conversation);
                    makeConversationList();
                }
            }
            /*for (Label label: chatListView.getItems()) {
                if (label == User.getLabelFromId.get(newMessage.getConversationId())) {
                }
            }*/
        }
    }
    private boolean isInExistingConversations(long conversation) {
        for (ConversationInfo conversationInfo: User.conversations) {
            if (conversationInfo.getId() == conversation) {
                return true;
            }
        }
        return false;
    }


    private void putMessageToRight (Message message) {

        String user = ":" + User.username + "\n";
        HBox hBox = new HBox(16);
        TextFlow textFlow = new TextFlow();
        TextFlow resultTextFlow;

        if (!message.isContainsFile()){
            String content = message.getContent() + "\n";
            Text text = new Text(user + content);
            textFlow.getChildren().add(text);
        }else {
            String downloadFile = " You Sent A File";
            Text text = new Text(user + downloadFile);
            Button downloadButton = new Button();
            downloadButton.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            downloadButton.setText(message.getContent());
            downloadButton.setGraphic(mainView.DOWNLOAD_BUTTON);
            downloadButton.setPrefSize(35, 35);
            downloadButton.setOnAction(e -> {
                try {
                    download(downloadButton.getText());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            });
            textFlow.getChildren().add(downloadButton);
            textFlow.getChildren().add(text);
        }

        Circle picture = new Circle(32, 32, 16);
        picture.setStroke(Color.TAN);
        try {
            /*Image image;
            if (User.profPic == null) {
                image = User.gender.equals("male") ? mainView.MALE_USER : mainView.FEMALE_USER;
            }else {
                image = User.profPic;
            }*/
            picture.setFill(new ImagePattern(profilePicture.getImage()));
        } catch (Exception e) {
            System.out.println("cannot find the image!!!!!");
            e.printStackTrace();
        }
        textFlow.setMaxWidth(500);
        textFlow.setTextAlignment(TextAlignment.RIGHT);
        resultTextFlow = new TextFlow(textFlow);

        hBox.getChildren().add(resultTextFlow);
        hBox.getChildren().add(picture);
        hBox.setAlignment(Pos.TOP_RIGHT);

        Platform.runLater(() -> {
            chatBox.getChildren().addAll(hBox);
            chatBox.setAlignment(Pos.BOTTOM_CENTER);
        });
    }

    private void putMessageToLeft (Message message) {

        String leftUser = contactName.getText() + ":\n";

        HBox hBox = new HBox(16);
        TextFlow textFlow = new TextFlow();
        TextFlow resultTextFlow;

        if (!message.isContainsFile()) {
            String content = message.getContent() + "\n";
            Text text = new Text(leftUser + content);
            textFlow.getChildren().add(text);
        }else {
            String downloadFile = "Sent You A File ";
            Text text = new Text(leftUser + downloadFile);
            Button downloadButton = new Button();
            downloadButton.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            downloadButton.setText(message.getContent());
            downloadButton.setGraphic(mainView.DOWNLOAD_BUTTON);
            downloadButton.setPrefSize(35, 35);
            downloadButton.setOnAction(e -> {
                try {
                    download(downloadButton.getText());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            });
            textFlow.getChildren().add(text);
            textFlow.getChildren().add(downloadButton);
        }

        textFlow.setMaxWidth(500);
        textFlow.setTextAlignment(TextAlignment.LEFT);
        resultTextFlow = new TextFlow(textFlow);
        Circle picture = new Circle(32, 32, 16);
        picture.setStroke(Color.TAN);
        try {
            Image image = contactImage.getImage();
            /*for (ConversationInfo conversation :User.conversations){
                if (conversation.getId() == User.currentConversationId) {
                    MemberInfo contact = conversation.getMembers().get(0).getAccountName().equals(User.accountName)?
                                         conversation.getMembers().get(1): conversation.getMembers().get(0);
                    if (contact.getProfilePic() == null) {
                        image = contact.getGender().equals("male")? mainView.MALE_USER: mainView.FEMALE_USER;
                    }else {
                        image = new Image(BASE_64.decodeBase64ToStream(contact.getProfilePic()));
                    }
                }
            }*/
            picture.setFill(new ImagePattern(image));
        } catch (Exception e) {
            System.out.println("problem getting contact's image!!!!!");
            e.printStackTrace();
        }
        hBox.getChildren().add(picture);
        hBox.getChildren().add(resultTextFlow);
        hBox.setAlignment(Pos.TOP_LEFT);

        Platform.runLater(() -> {
            chatBox.getChildren().add(hBox);
            chatBox.setAlignment(Pos.BOTTOM_CENTER);
        });
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private void download (String fileString) throws IOException {
        System.out.println("Downloading file...");       // todo remove

        String dir = "D:\\Java projects\\FX\\Messenger client\\src\\Downloads\\";
        int numOfFiles = Objects.requireNonNull(new File(dir).listFiles()).length;
        numOfFiles++;
        File targetFile = new File(dir + numOfFiles);
        InputStream inStream = new ByteArrayInputStream(Base64.getDecoder().decode(fileString));
        String mimeType = URLConnection.guessContentTypeFromStream(inStream);

        if (mimeType.contains("/")) {
            mimeType = "." + mimeType.substring(mimeType.indexOf('/') + 1);
            File downloadedFile = new File(dir + targetFile.getName() + mimeType);
            FileOutputStream fileOut = new FileOutputStream(downloadedFile);
            IOUtils.copy(inStream, fileOut);
            System.out.println(downloadedFile.getName() + " downloaded successfully.");
            fileOut.close();
        }else {
            File downloadedFile = new File(dir + targetFile.getName());
            FileOutputStream fileOut = new FileOutputStream(downloadedFile);
            IOUtils.copy(inStream, fileOut);
            System.out.println(targetFile.getName() + " downloaded successfully.");
        }
        inStream.close();
    }


    public void setupSelectedConversation (ImageView profile, String userName/*, boolean onlineStatus*/) {
        if (profile.getImage() != contactImage.getImage() || !userName.equals(contactName.getText())) {
            chatBox.getChildren().clear();
        }
        setContactImage(profile);
        setContactName(userName);

        for (Message message: User.thisConversationMessages) {
            if (message.getSender().equals(User.accountName)) {
                putMessageToRight(message);
            } else {
                putMessageToLeft(message);
            }
        }
    }

    public Image formatImage (Image image, String gender) {
        if (image == null || Math.min(image.getWidth(), image.getHeight()) == 0) {
            defaultPic = gender.equals("male") ? mainView.MALE_USER : mainView.FEMALE_USER;
            return defaultPic;
        }else {
            int squareSide = (int) Math.min(image.getWidth(), image.getHeight());
            int x = (int) ((image.getWidth() - squareSide) / 2);
            int y = (int) ((image.getHeight() - squareSide) / 2);
            System.out.println("" + x + y + squareSide);
            PixelReader reader = image.getPixelReader();
            return new WritableImage(reader, x, y, squareSide, squareSide);
        }
    }

    public Rectangle formatClip() {
        Rectangle rect = new Rectangle(63, 63);
        rect.setArcHeight(63);
        rect.setArcWidth(63);
        return rect;
    }

    public Rectangle formatSmallClip() {
        Rectangle rect = new Rectangle(30, 30);
        rect.setArcHeight(30);
        rect.setArcWidth(30);
        return rect;
    }

    private void setContactImage (ImageView profilePicture)  {
        contactImage.setClip(formatClip());
        contactImage.setImage(profilePicture.getImage());
    }


    private void setContactName (String name) {
        contactName.setText(name);
    }


    public void makeConversationList() throws IOException {

        //todo remove
        int iteration = 0;

        List<Label> list = new ArrayList<>();
        Collections.reverse(list);

        chatListView.getItems().clear();

        if (User.conversations != null) {
            for (ConversationInfo conversation : User.conversations) {
                Label conversationLabel = new Label();
                if (conversation.getType().equals(conversation.GROUP_CHAT)) {
                    //TODO:
                    // *important* MAP IDs TO USERNAMES
                    Image gpImage;
                    if (conversation.getConversationPic() != null) {
                        gpImage = formatImage(new Image(BASE_64.decodeBase64ToStream(conversation.getConversationPic())), "");
                    } else {
                        gpImage = mainView.GROUP_CHAT;
                    }
                    ImageView profile = new ImageView(gpImage);
                    profile.setClip(formatSmallClip());
                    profile.setFitHeight(30);
                    profile.setFitWidth(30);
                    conversationLabel.setGraphic(profile);                                      //  pic

                    conversationLabel.setText(conversation.getConversationName());                 // name
                    conversationLabel.setTextFill(Color.SNOW);
                    conversationLabel.setFont(Font.font(18));

                    //int labId = conversationLabel.getId();
                    User.getIdFromLabel.put(conversationLabel, conversation.getId());//id (map to label)
                    User.getLabelFromId.put(conversation.getId(), conversationLabel);//label (map to id)

                    //chatListView.getItems().add(conversationLabel);
                    list.add(conversationLabel);
                } else if (conversation.getType().equals(conversation.PRIVATE_CHAT)) {
                    /*if (conversation.isOnline()){
                        ImageView online = new ImageView(new Image(getClass().getResourceAsStream(mainView.ONLINE4)));
                        online.setFitHeight(24);
                        online.setFitWidth(24);
                        conversationLabel.getChildren().add(online);// 0
                    }*/
                    MemberInfo contact = conversation.getMembers().get(0).getAccountName().equals(User.accountName) ?
                            conversation.getMembers().get(1) :
                            conversation.getMembers().get(0);
                    Image image;
                    ImageView profile = new ImageView();
                    if (contact.getProfilePic() != null) {
                        image = new Image(BASE_64.decodeBase64ToStream(contact.getProfilePic()));
                        image = formatImage(image, contact.getGender());
                        profile.setImage(image);
                        profile.setClip(formatSmallClip());

                    } else {
                        image = contact.getGender().equals("male") ? mainView.MALE_USER : mainView.FEMALE_USER;
                        profile.setImage(image);
                    }
                    profile.setFitHeight(30);
                    profile.setFitWidth(30);
                    conversationLabel.setGraphic(profile);                                          // pic

                    conversationLabel.setText(contact.getUserName());                              // username
                    conversationLabel.setTextFill(Color.SNOW);
                    conversationLabel.setFont(Font.font(18));

                    User.getIdFromLabel.put(conversationLabel, conversation.getId());  // map id to label
                    User.getLabelFromId.put(conversation.getId(), conversationLabel);//label (map to id)

                    //chatListView.getItems().add(conversationLabel);
                    list.add(conversationLabel);
                }
                //System.out.println("make listView: " + iteration++);
            }
           // System.out.println("list size: " + list.size());
            ObservableList<Label> observable = FXCollections.observableArrayList(list);
            chatListView.setItems(observable);
            //System.out.println("chatListView size: " + chatListView.getItems().size());
        }
    }


    private ArrayList<Contact> rapidlyGetAllContactsFromServer () {
        return new ArrayList<>();
    }



    private boolean isOnline;
    public void online(boolean connectionStatus) {
        isOnline = connectionStatus;
    }
    private void updateContactOnlineStatus () {
        if (isOnline) {
            contactOnlineStatus.setVisible(true);
            onlineText.setVisible(true);
        }else {
            contactOnlineStatus.setVisible(false);
            onlineText.setVisible(false);
        }
    }


    public static AtomicBoolean previousTypingThreadRunning = new AtomicBoolean(false);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public synchronized void thisContactIsTyping () {
        Thread typing = new Thread() {
            @Override
            public void run() {
                previousTypingThreadRunning.set(true);
                showIsTyping.setVisible(true);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                showIsTyping.setVisible(false);
                previousTypingThreadRunning.set(false);
            }
        };
        typing.start();
    }
    private void thisUserIsTyping () throws IOException {
        //connection.sendData(new TypingStatus());
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public static final MainController mainController = new MainController();
    //private final ConnectToServerReceiver connection = ConnectToServerReceiver.ConnectToServerReceiver;
    private final ConnectToServer connection = ConnectToServer.connectToServer;

    private boolean minimized = false;
    private ViewFactory mainView = ViewFactory.mainViewFactory;
    private Image defaultPic;




    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        mainView.buildMainSceneImages();
        buttonEmoji.setGraphic(mainView.EMOJI_BUTTON);
        sendButton.setGraphic(mainView.SEND_BUTTON);
        attachButton.setGraphic(mainView.ATTACH_BUTTON);
        searchContactIcon.setImage(mainView.SEARCH_VIEW);
        for(Node text : emojiTextFlow.getChildren()){
            text.setOnMouseClicked(event -> {
                typeHereTextField.setText(typeHereTextField.getText()/*+" "*/+((Text)text).getText());
            });
        }
        scrollPane.vvalueProperty().bind(chatBox.heightProperty());
        chatListView.setStyle("-fx-control-inner-background: #4b6777");

        showUserName.setText(User.username);
        profilePicture.setClip(formatClip());
        profilePicture.setImage(formatImage(User.profPic, User.gender));

        online(true);//todo remove
        Image onlineImage = new Image(getClass().getResourceAsStream(mainView.ONLINE4));
        contactOnlineStatus.setImage(onlineImage);
        //TODO: call when chat clicked
        contactOnlineStatus.setVisible(false);
        onlineText.setVisible(false);
        setContactName("");

        try {
            makeConversationList();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("\ncannot make conversation list !");
        }

        // SECOND THREAD CONNECTION PROBLEM:
        /*try {
            receiveNewMessages();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        typeHereTextField.setOnAction(e -> {
            try {
                sendAction(e);
            } catch (IOException | InterruptedException ex) {
                ex.printStackTrace();
            }
        });


        try {
            connection.incomingMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }
        connection.observableNewMessages.addListener(new ListChangeListener<NewMessage>() {
             @Override
             public void onChanged(Change<? extends NewMessage> change) {
                 System.out.println("change listener called");
                 change.next();
                 if (change.getAddedSize() > 0) {
                     ObservableList<NewMessage> messages = connection.observableNewMessages;
                     while (messages.size() > 0) {
                         NewMessage newMessage = messages.get(messages.size() - 1);
                         try {
                             handleNewMessage(newMessage);
                         } catch (IOException | ClassNotFoundException e) {
                             e.printStackTrace();
                         }
                         messages.remove(messages.size() - 1);
                     }
                 }
             }
         });




        //todo
        // UNComment >>>>>>>>>>>>>>>>>>>>>>>>>
        /*typeHereTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldStr, String newStr) {
                if (!previousTypingThreadRunning.get()) {
                    try {
                        thisUserIsTyping();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });*/



       /* chatListView.setOnMouseClicked(e -> {
            HBox thisConversation = chatListView.getSelectionModel().getSelectedItem();

            setupSelectedConversation ((ImageView)thisConversation.getChildren().get(0), // prof pic
                             thisConversation.getChildren().get(1).getAccessibleText()); // username

            long id = Long.parseLong(thisConversation.getChildren().get(2).getAccessibleText());
            RequestConversation requestConversation = new RequestConversation(id);
            User.currentConversationId = id;
            try {
                connection.sendData(requestConversation);
                User.thisConversationMessages = ((AllMessages) connection.getData()).getAllMessages();
            } catch (IOException | ClassNotFoundException ex) {
                ex.printStackTrace();
                System.out.println("\nrequest for conversation failed! ");
            }
        });*/

        chatListView.setCellFactory(slv -> {
            ListCell<Label> cell = new ListCell<Label>(){
                @Override
                protected void updateItem(Label item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        if (item.getGraphic() != null) {
                            setGraphic(item.getGraphic());
                        }
                        setText(item.getText());
                        setTextFill(Color.SNOW);
                        setFont(Font.font(18));
                    }
                }
            };
            cell.setOnMouseClicked(e -> {
                if (cell.getItem() != null) {
                    /*System.out.println("clicked target user name: " + cell.getItem().getText());//todo remove
                    System.out.println("clicked target label: " + cell.getItem());//todo remove*/
                    chatBox.getChildren().clear();
                    try {
                        User.currentConversationId = User.getIdFromLabel.get(cell.getItem());
                        connection.sendData(new RequestConversation(User.getIdFromLabel.get(cell.getItem())));
                        AllMessages messages = (AllMessages) connection.getData();
                        User.thisConversationMessages = messages.getAllMessages();

                        updateContactOnlineStatus();//todo handle

                        for (ConversationInfo conversation : User.conversations) {
                            if (conversation.getId() == User.getIdFromLabel.get(cell.getItem())) {
                                MemberInfo contact = conversation.getMembers().get(0).getAccountName().equals(User.accountName)?
                                        conversation.getMembers().get(1): conversation.getMembers().get(0);
                                System.out.println("contact selected on the main scene: " + contact.getAccountName());
                                Image image = contact.getProfilePic() != null?
                                        new Image(BASE_64.decodeBase64ToStream(contact.getProfilePic())): null;
                                ImageView picView = new ImageView(formatImage(image, contact.getGender()));
                                picView.setClip(formatClip());

                                setupSelectedConversation(picView, contact.getUserName());
                            }
                        }
                    }catch (IOException | ClassNotFoundException ex) {
                        ex.printStackTrace();
                    }
                }
               // getNewMessagesPeriodically();
            });
            return cell;
        });

        chatListView.setOnMouseClicked(e -> {
        });




        /*MainClient.mainStage.iconifiedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                if (t1) {
                    minimized = true;
                }
                System.out.println("minimized:" + t1); // todo REMOVE this line
            }
        });

        MainClient.mainStage.maximizedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                if (t1) {
                    minimized = false;
                }
                System.out.println("maximized:" + t1);
            }
        });*/
    }
}
