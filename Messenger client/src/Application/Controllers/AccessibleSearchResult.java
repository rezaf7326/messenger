package Application.Controllers;

import Application.Network.Utils.BASE_64;
import Application.View.ViewFactory;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.MemberInfo;
import model.SearchResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

//SINGLETON
public class AccessibleSearchResult {

    private AccessibleSearchResult () {}

    public static HashMap<String, String> getUserFromAccName = new HashMap<>();
    public static ArrayList<Label> resultList = new ArrayList<>();

    //public static boolean existsInChatList = false;
    //public static boolean searchFinished = false;
    public static MemberInfo memberResult;

    public static FXMLLoader mainLoader;




    public static void listAllResults (SearchResult result) {
        final MainController control = MainController.mainController;
        final ViewFactory mainView = ViewFactory.mainViewFactory;


        for (MemberInfo member : result.getResult()) {
            Label memberLabel = new Label();
            Image image;
            ImageView profile;
            try {
                if (member.getProfilePic() != null) {
                    image = new Image(BASE_64.decodeBase64ToStream(member.getProfilePic()));
                    profile = new ImageView(control.formatImage(image, member.getGender()));
                } else {
                    image = member.getGender().equals("male") ? mainView.MALE_USER : mainView.FEMALE_USER;
                    profile = new ImageView(image);
                }

                profile.setClip(control.formatSmallClip());
                profile.setFitHeight(30);
                profile.setFitWidth(30);
                memberLabel.setGraphic(profile);                        // profile

                getUserFromAccName.put(member.getAccountName(), member.getUserName()); //mapping username with account name

                memberLabel.setText(member.getAccountName());             // acc name
                memberLabel.setTextFill(Color.SNOW);
                memberLabel.setFont(Font.font(16));

            }catch (IOException io) {
                io.printStackTrace();
            }
            resultList.add(memberLabel);
        }
    }

}
