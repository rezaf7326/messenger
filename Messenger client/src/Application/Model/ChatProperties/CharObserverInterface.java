package Application.Model.ChatProperties;

import Application.Model.User;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface CharObserverInterface extends Remote {

    interface ChatObserverInterface extends Remote{
        boolean update(String username,String message) throws RemoteException;
        ArrayList<User> getOnlineUsers() throws RemoteException;
        String getUsername() throws RemoteException;
        boolean updateUI(ArrayList<String> clientList) throws RemoteException;
    }
}
