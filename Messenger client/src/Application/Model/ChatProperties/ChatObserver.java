package Application.Model.ChatProperties;

import Application.Model.ChatProperties.CharObserverInterface.ChatObserverInterface;
import Application.Model.User;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class ChatObserver extends UnicastRemoteObject implements ChatObserverInterface {

    ChatObserver observer;

    public ChatObserver (ChatObserver observer) throws RemoteException {
        super();
        this.observer = observer;
    }

    @Override
    public boolean update(String username, String message) throws RemoteException {
        return observer.update(username, message);
    }

    @Override
    public ArrayList<User> getOnlineUsers() throws RemoteException {
        return observer.getOnlineUsers();
    }

    @Override
    public String getUsername() throws RemoteException {
        return observer.getUsername();
    }

    @Override
    public boolean updateUI(ArrayList<String> clientList) throws RemoteException {
        return observer.updateUI(clientList);
    }
}
