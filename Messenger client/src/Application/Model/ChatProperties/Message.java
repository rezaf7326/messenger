package Application.Model.ChatProperties;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;



public class Message {


    private SimpleStringProperty sender;
    private SimpleStringProperty content; // if not text: store -> Address
    private SimpleStringProperty date;
    private SimpleStringProperty size;
    private SimpleBooleanProperty isLastMessage;// ---> ONLY for the last message is TRUE (all others: FALSE)
    // ****NOTE****: if the last message is other than a text,
    //  then show one of {"Photo", "GIF", "Voice", "Video"} + read/not-read status
    //  in the contact bar (inside contacts table view)...
    //  Otherwise, show content(string)




    // CONSTRUCTOR
    public Message (String sender, String content, int size, boolean isLastMessage) {
        this.sender = new SimpleStringProperty(sender);
        this.content = new SimpleStringProperty(content);
        this.date = new SimpleStringProperty(timeToStringDate());  // use of method
        this.size = new SimpleStringProperty(formatSize(size));    // use of method
        this.isLastMessage = new SimpleBooleanProperty(isLastMessage);
    }




    // GETTERS
    public String getSender() {
        return sender.get();
    }
    public String getContent() {
        return content.get();
    }
    public String getDate() {
        return date.get();
    }
    public String getSize() {
        return size.get();
    }
    public boolean isIsLastMessage() {
        return isLastMessage.get();
    }






    // METHODS
    private String timeToStringDate () {
            /*TODO
                    modify how to show the time (if long past only date[April 15] otherwise only time[like: 3:44 PM])
             */
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        //System.out.println(dateTimeFormatter.format(now)); // WORKS FINE!
        return dateTimeFormatter.format(now);
    }

    private String formatSize (int size) {
        String stringSize = "";
        if (size <= 0){
            stringSize = "0";
        }else if(size < 1024) {
            stringSize = size + " B";
        }else if (size < 1048576) {
            stringSize = (size / 1024) + " KB";
        }else {
            stringSize = (size / 1048576) + " MB";
        }
        return stringSize;
    }

}
