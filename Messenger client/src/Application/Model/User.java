package Application.Model;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import model.ConversationInfo;
import model.Message;

import java.util.ArrayList;
import java.util.HashMap;

// Singleton:
public class User {

    public static String username="";
    public static String gender = "";
    public static String accountName ="";
    public static Image profPic = null;
    public static Long currentConversationId = null;
    public static ArrayList<Message> thisConversationMessages;
    public static ArrayList<String> contactsAccNames;
    public static ArrayList<ConversationInfo> conversations = null;
    public static HashMap<Label, Long> getIdFromLabel = new HashMap<>();
    public static HashMap<Long, Label> getLabelFromId = new HashMap<>();

    public static void removeAll () {
        username = "";
        gender = "";
        accountName ="";
        profPic = null;
        currentConversationId = null;
        thisConversationMessages = null;
        contactsAccNames = null;
        conversations = null;
        getIdFromLabel = new HashMap<>();
    }

    private User() {}
}
