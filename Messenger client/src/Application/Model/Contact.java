package Application.Model;

import model.Message;

import java.io.File;
import java.util.ArrayList;

public class Contact {

    private String userName = "";
    private String id = "";
    private ArrayList<Message> chatHistory;
    private String profilePicture = null;
    private boolean onlineStatus = false;


    //  CONSTRUCTOR:
    public Contact(String contactId) {
        this.id = contactId;
    }


    //GETTER s & SETTER s
    public String getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }
    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public ArrayList<Message> getChatHistory() {
        return chatHistory;
    }
    public void setChatHistory(ArrayList<Message> chatHistory) {
        this.chatHistory = chatHistory;
    }

    public boolean isOnline() {
        return onlineStatus;
    }
    public void setOnlineStatus(boolean onlineStatus) {
        this.onlineStatus = onlineStatus;
    }
}
