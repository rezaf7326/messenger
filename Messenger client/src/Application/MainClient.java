package Application;

import Application.Network.ConnectToServer;
import Application.View.ViewFactory;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

import java.io.IOException;


public class MainClient extends Application {


    private static MainClient app = new MainClient();
    private static ViewFactory mainView = ViewFactory.mainViewFactory;
    private static ConnectToServer connection = ConnectToServer.connectToServer;

    public final String appName = "Messenger.v1.01.exe";
    public final Image appIcon = new Image(getClass().getResourceAsStream("app icon/messenger icon.png"));
    public final Image appIcon2 = new Image(getClass().getResourceAsStream("app icon/messenger icon2.png"));
    public final Image appIcon3 = new Image(getClass().getResourceAsStream("app icon/messenger icon3.png"));

    private static Stage loginStage;
    private static Stage newAccountStage;
    public static Stage mainStage;
    public static Stage searchStage = null;


    public static void showLoginStage () {
        Scene scene = mainView.getLoginScene();
        loginStage = new Stage();
        loginStage.setScene(scene);
        loginStage.setTitle(app.appName);
        loginStage.getIcons().add(app.appIcon2);
        loginStage.initStyle(StageStyle.UNDECORATED);
        loginStage.show();
    }
    public static void closeLoginStage () {
        loginStage.close();
        loginStage =  null;
    }

    public static void showCreateNewAccountStage () {
        Scene scene = mainView.getCreateNewAccountScene();
        newAccountStage = new Stage();
        newAccountStage.setScene(scene);
        newAccountStage.setTitle(app.appName);
        newAccountStage.getIcons().add(app.appIcon2);
        newAccountStage.initStyle(StageStyle.UNDECORATED);
        newAccountStage.show();
    }
    public static void closeCreateNewAccountStage () {
        newAccountStage.close();
    }

    public static void showMainStage () {
        Scene scene = mainView.getMainScene();
        mainStage = new Stage();
        mainStage.setScene(scene);
        mainStage.setTitle(app.appName);
        mainStage.getIcons().add(app.appIcon2);
        mainStage.show();
        mainStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                closeConnection();
            }
        });
    }
    public static void closeMainStage () {
        logout();
        mainStage.close();
    }

    public static void showSearchStage () {
        if (searchStage == null) {
            Scene scene = mainView.getSearchScene();
            searchStage = new Stage();
            searchStage.setScene(scene);
            searchStage.getIcons().add(app.appIcon2);
            searchStage.initStyle(StageStyle.UNDECORATED);
            searchStage.show();
        }
    }
    public static void closeSearchStage () {
        searchStage.close();
        searchStage = null;
    }

    public static void closeConnection() {
        try {
            connection.sendData("close connection");
            connection.authentication("close connection");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void logout () {
        try {
            connection.sendData("logging out");
            connection.authentication("logging out");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage primaryStage) {
        showLoginStage();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
